const mongoose = require('mongoose');

const AlbumSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    artist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artist',
        required: true
    },
    AlbumImage: String,
    releaseDate: {
        type: Date
    }
});

const Album = mongoose.model('Album', AlbumSchema);
module.exports = Album;