const express = require('express');
const Track = require("../models/Track");

const router = express.Router();

router.post('/', async (req, res) => {
    try {
        const trackData = req.body;

        if(trackData.album.length === 0) {
            res.status(400).send({error: 'album should be filled'});
        }

        if (trackData.name.length === 0) {
            res.status(400).send({error: 'name should be filled'});
        }

        const track = new Track(trackData);
        await track.save();
        res.send(track);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/', async (req, res) => {
    try {
        const criteria = {};
        if (req.query.album) {
            criteria.album = req.query.album;
        }

        const tracksData = await Track.find(criteria);
        res.send(tracksData);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;