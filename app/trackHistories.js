const express = require('express');

const TrackHistory = require('../models/TrackHistory');
const User = require("../models/User");

const router = express.Router();

router.post('/', async (req, res) => {

    const token = req.get('Authorization');

    if (!token) {
        return res.status(401).send({error: 'No token present'});
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.status(401).send({error: 'Wrong token!'});
    }

    try {
        const trackHistory = new TrackHistory(req.body);
        trackHistory.user = user._id;
        trackHistory.datetime = new Date();

        await trackHistory.save();
        return res.send(trackHistory);
    } catch (error) {
        return res.status(400).send(error);
    }
});

module.exports = router;